import serial
import cv2
import math
import time
from grip_red import RedPipeline
from grip_blue import BluePipeline

fov_v = 48.8
fov_w = 62.2
pic_v = 240
pic_w = 320

target_size_in = 5.25

def sign(x):
    if x > 0:
        return 1
    elif x < 0:
        return -1
    return 0

def calc_checksum(data):
    return abs((0xFF - sum(data)) % 256)

def read_packet(conn):
    byte = conn.read(1)
    if byte == b'\xFA':
        pktId = conn.read(1)
        data = conn.read(4)
        checksum = conn.read(1)
        if len(data) == 4 and len(checksum) == 1 and ord(checksum) == calc_checksum(data):
            return pktId, struct.unpack("<i", data)[0]
    return None

def write_packet(id, data):
    packet = b'\xFA'
    packet += id.to_bytes(1, byteorder='little', signed=False)
    body = data.to_bytes(4, byteorder='little', signed=True)
    packet += body
    packet += calc_checksum(body).to_bytes(1, byteorder='little', signed=False)
    return packet

if __name__ == '__main__':
    conn = serial.Serial("/dev/ttyACM1", baudrate=115200, timeout=0.0, write_timeout=0)
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, pic_v)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, pic_w)

    #print("Waiting for color packet")
    #color = 0
    #while color == 0:
    #    packet = read_packet(conn)
    #    if packet is not None:
    #        id, data = packet
    #        if id == 0x03:
    #            color = data
    #    time.sleep(20/1000)

    #if color == 1:
    #    pipeline = BluePipeline()
    #else:
    #    pipeline = RedPipeline()
    pipeline = RedPipeline()

    while True:
        conn.read_all()
        ret, frame = cap.read()
        if not ret:
            continue
        pipeline.process(frame)

        contours = pipeline.filter_contours_output
        if (len(contours) == 0):
            print("Didn't find target")
            continue

        largest = sorted(contours, key=lambda C: cv2.contourArea(C), reverse=True)[0]
        # Math to find angle   
        M = cv2.moments(largest)
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])

        dx = cx - pic_w / 2  # dx is position relative to center
        dir_ = sign(dx)
        dx = abs(dx)
        ratio = dx / (pic_w / 2)
        angle_diff = dir_ * math.atan(ratio * math.tan(math.radians(fov_w / 2)))
        angle_diff = fov_w / 2 - math.degrees(angle_diff) - 8 # Camera offset angle
        print("Angle: {} degrees".format(angle_diff))
        conn.write(write_packet(0x01, int(angle_diff*10)))

        # Math to find distance
        x,y,w,h = cv2.boundingRect(largest)
        theta_y = fov_v * h / pic_v
        distance_in = 2 * target_size_in / math.tan(math.radians(theta_y))
        print("Distance: {} in".format(distance_in))
        conn.write(write_packet(0x02, int(distance_in)))
