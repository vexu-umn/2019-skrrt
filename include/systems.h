
#pragma once

#include "gopher/ramsete_controller.h"
#include <queue>
#include "gopher/pose_estimator.h"
#include "okapi/api.hpp"
#include "gopher/system.h"

using namespace okapi::literals;

#define DRIVE_WHEEL_DIA 4_in
#define DRIVE_WHEELBASE 5.37_in
#define DRIVE_GEARSET okapi::AbstractMotor::gearset::green

class Intake: public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> motor;
    int state;

    void loop() override;

    public:
    void intake();
    void unstick();
    void idle();

    Intake();

    static const int STATE_IDLE = 0;
    static const int STATE_INTAKE = 1;
    static const int STATE_UNSTICK = 2;
};
// todo replace with okapi pid controller
#define PUNCHER_MOVE_KP 0.1

class Puncher : public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> articulate;
    std::shared_ptr<okapi::AbstractMotor> punch;
    int state;
    float angleTarget;
    float articulateSpeed;

    void loop() override;

    public:
    void doPunch();
    void aimAt(float angle);
    void idle();
    void moveArticulate(float speed);
    void setPunch();
    void velPunch();

    Puncher();

    static const int STATE_IDLE = 0;
    static const int STATE_PUNCHING = 1;
};

class PushyArm : public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> articulate;

    void loop() override;

    public:
    PushyArm();

    void articulateSpeed(float speed);
    void articulateAngle(float angle);

    void waitUntilSettled();
};

class DescoreArm : public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> articulate;
    void loop() override;

    public:
    DescoreArm();
    void articulateSpeed(float speed);
};