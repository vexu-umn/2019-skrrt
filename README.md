Skrrt
===
Skrrt is VexU team UMN's 15" robot for the 2018-19 game Turning Point.

Developing
===
Skrrt code depends on Gopherlib.
```
git clone https://gitlab.com/vexu-umn/skrrt.git
prosv5 conduct apply --force-apply okapilib
prosv5 conduct apply --force-apply kernel
prosv5 conduct apply --force-apply gopherlib
make
prosv5 upload
```