#include "systems.h"
#include "gopher/utils.h"
#include "okapi/api.hpp"

Puncher::Puncher() {
    this->articulate = std::make_shared<okapi::Motor>(16);
    this->punch = std::make_shared<okapi::MotorGroup>(std::initializer_list<okapi::Motor>({okapi::Motor(14), okapi::Motor(-15)}));
    this->punch->setBrakeMode(okapi::AbstractMotor::brakeMode::brake);
    this->punch->setEncoderUnits(okapi::AbstractMotor::encoderUnits::rotations);
    this->articulate->setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
    this->articulate->setEncoderUnits(okapi::AbstractMotor::encoderUnits::degrees);
};

void Puncher::idle() {
    this->punch->moveVelocity(0);
}

void Puncher::doPunch() {
    this->punch->moveRelative(1, getVelocityFromGearset(this->punch->getGearing()));
}

void Puncher::velPunch() {
    this->punch->moveVelocity(getVelocityFromGearset(this->punch->getGearing()));
}

void Puncher::setPunch() {
    this->punch->moveRelative(0.5, getVelocityFromGearset(this->punch->getGearing()));
}

void Puncher::aimAt(float targetAngle) {
    this->articulate->moveAbsolute(targetAngle, getVelocityFromGearset(this->articulate->getGearing()));
}

void Puncher::moveArticulate(float speed) {
    this->articulateSpeed = speed;
}

void Puncher::loop() {
}