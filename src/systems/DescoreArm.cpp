#include "systems.h"
#include "okapi/api.hpp"
#include "gopher/utils.h"

DescoreArm::DescoreArm() {
    this->articulate = std::make_shared<okapi::Motor>(19);
    this->articulate->setGearing(okapi::AbstractMotor::gearset::green);
    this->articulate->setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
}

void DescoreArm::articulateSpeed(float speed) {
    this->articulate->moveVelocity(speed * getVelocityFromGearset(this->articulate->getGearing()));
}

void DescoreArm::loop() {
    
}