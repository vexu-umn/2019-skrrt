#include "systems.h"
#include "okapi/api.hpp"
#include "gopher/utils.h"

PushyArm::PushyArm() {
    this->articulate = std::make_shared<okapi::Motor>(20);
    this->articulate->setGearing(okapi::AbstractMotor::gearset::red);
    this->articulate->setEncoderUnits(okapi::AbstractMotor::encoderUnits::degrees);
    this->articulate->setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
    this->articulate->tarePosition();
}

void PushyArm::articulateSpeed(float speed) {
    this->articulate->moveVelocity(speed * getVelocityFromGearset(this->articulate->getGearing()));
}

void PushyArm::articulateAngle(float degrees) {
    this->articulate->moveAbsolute(degrees, getVelocityFromGearset(this->articulate->getGearing()));
}

void PushyArm::waitUntilSettled() {
    double error = this->articulate->getTargetPosition() - this->articulate->getPosition();
    while (!this->time->getSettledUtil()->isSettled(error)) {
        error = this->articulate->getTargetPosition() - this->articulate->getPosition();
        this->time->getRate()->delayUntil(1_ms);
    }
}

void PushyArm::loop() {
    
}