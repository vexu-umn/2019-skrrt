#include "systems.h"
#include "okapi/api.hpp"

void Intake::intake() {
    this->state = Intake::STATE_INTAKE;
}

void Intake::unstick() {
    this->state = Intake::STATE_UNSTICK;
}

void Intake::idle() {
    this->state = Intake::STATE_IDLE;
}

Intake::Intake() {
    this->motor = std::make_shared<okapi::MotorGroup>(std::initializer_list<okapi::Motor>({okapi::Motor(11), okapi::Motor(-13)}));
    this->motor->setGearing(okapi::AbstractMotor::gearset::blue);
}

void Intake::loop() {
    switch(this->state) {

        case Intake::STATE_INTAKE:
        this->motor->moveVelocity(600);
        break;

        case Intake::STATE_UNSTICK:
        this->motor->moveVelocity(-600);
        break;

        default:
        case Intake::STATE_IDLE:
        this->motor->moveVelocity(0);
    }
}