#include "main.h"
#include "systems.h"
#include "gopher/utils.h"
#include "gopher/drivetrain.h"
#include "pros/apix.h"
#include <iostream>
#include <fstream>
#include "gopher/comm.h"

#define RASPI_CONN

using namespace okapi::literals;

std::shared_ptr<TankDrive> drive;
std::shared_ptr<Puncher> puncher;
std::shared_ptr<Intake> intake;
std::shared_ptr<PushyArm> arm;
std::shared_ptr<DescoreArm> descoreArm;

std::vector<std::pair<std::string, auton_ptr>> autons;
std::shared_ptr<ScreenInterface> interface;

std::shared_ptr<std::ifstream> raspiConn;
std::shared_ptr<pros::Task> raspiTask;
float targetAngle = 0;
float targetDist;

#define PACKET_VISION_ANGLE 0x01
#define PACKET_VISION_DIST 0x02
#define PACKET_COLOR_DETECT 0x03
void recvVisionData(uint8_t packetId, int32_t data);

class VisionPoseReporter : public PoseEstimator {
	private:
	float angle;
	public:
	void setAngle(float angle);
	Pose getPose() override;
};

void VisionPoseReporter::setAngle(float angle) {
	this->angle = angle;
};

Pose VisionPoseReporter::getPose() {
	return Pose(0 * okapi::meter, 0 * okapi::meter, this->angle * okapi::degree);
}

std::shared_ptr<VisionPoseReporter> reporter;

void applyInterfaceChange() {
}

void initialize() {
	// Init serial comms
	#ifdef RASPI_CONN

	CommHandler::getInstance().registerPacketHandler(PACKET_VISION_ANGLE, &recvVisionData);
	CommHandler::getInstance().registerPacketHandler(PACKET_VISION_DIST, &recvVisionData);
	#endif
	reporter = std::make_shared<VisionPoseReporter>();

	// Init systems
	std::shared_ptr<okapi::MotorGroup> leftMotors = std::make_shared<okapi::MotorGroup>(
        std::initializer_list<okapi::Motor>({okapi::Motor(7, true, DRIVE_GEARSET),
                                             okapi::Motor(8, true, DRIVE_GEARSET), 
                                             okapi::Motor(3, true, DRIVE_GEARSET)}));
    std::shared_ptr<okapi::MotorGroup> rightMotors = std::make_shared<okapi::MotorGroup>(
        std::initializer_list<okapi::Motor>({okapi::Motor(4, false, DRIVE_GEARSET),
                                             okapi::Motor(5, false, DRIVE_GEARSET), 
                                             okapi::Motor(6, false, DRIVE_GEARSET)}));
	leftMotors->setBrakeMode(okapi::AbstractMotor::brakeMode::brake);
	rightMotors->setBrakeMode(okapi::AbstractMotor::brakeMode::brake);
	leftMotors->setReversed(false);
	rightMotors->setReversed(true);
	drive = std::make_shared<TankDrive>(leftMotors, rightMotors, 4_in, 10.5_in);

	intake = std::make_shared<Intake>();
	puncher = std::make_shared<Puncher>();
	arm = std::make_shared<PushyArm>();
	descoreArm = std::make_shared<DescoreArm>();

	interface = std::make_shared<ScreenInterface>(&autons, reporter);

	autons.push_back(std::pair<std::string, auton_ptr>("Park", &drive_onto_platform));

	interface->initialize();

	// Init paths
	drive->preparePath({{0_in, 0_in, 0_deg}, {26_in, -2_in, -55_deg}}, "A_blue");
	drive->preparePath({{0_in, 0_in, 0_deg}, {5_in, 0_in, 0_deg}}, "B");
	drive->preparePath({{0_in, 0_in, 0_deg}, {10_in, 0_in, 0_deg}}, "C");
	drive->preparePath({{0_in, 0_in, 0_deg}, {22_in, 15_in, -5_deg}}, "D_blue");

}

void disabled() {}

void competition_initialize() {}

// Example auton function
void drive_onto_platform() {
	/*if (interface->getColor() == AllianceColor::blue) {
		drive->drivePath("A_blue");
	}
	else {
		drive->drivePath("A_red");
	}*/
	drive->pointToAngle(90_deg);
	drive->waitUntilSettled();
	std::cout << "Done" << std::endl;
};

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {
	#ifdef RASPI_CONN
	CommHandler::getInstance().sendPacket(PACKET_COLOR_DETECT, interface->getColor()+1);
	#endif
	//intake->intake();
	intake->idle();
	drive->drivePath("A_blue");
	drive->waitUntilSettled();
	puncher->setPunch();
	intake->intake();
	arm->articulateAngle(-110);
	pros::delay(500);
	drive->drivePathReverse("B");
	drive->waitUntilSettled();
	pros::delay(1000);
	puncher->aimAt(30);
	pros::delay(500);
	puncher->doPunch();
	pros::delay(500);
	puncher->aimAt(100);
	pros::delay(500);
	puncher->doPunch();
	arm->articulateAngle(-100);
	//drive->pointToAngle(10_deg);
	drive->drivePathReverse("C");
	drive->waitUntilSettled();
	arm->articulateAngle(0);
	drive->getChassis()->turnAngle(-55_deg);
	drive->drivePath("D_blue");
	pros::delay(1500);
	arm->articulateAngle(-110);
	drive->waitUntilSettled();
	arm->articulateAngle(-170);
	arm->waitUntilSettled();
	// Aim and shoot two
}

void opcontrol() {
	std::cout << "Op control starting" << std::endl;
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	while (true) {

		#ifdef RASPI_CONN
		CommHandler::getInstance().sendPacket(PACKET_COLOR_DETECT, interface->getColor()+1);
		#endif

		double power = master.get_analog(ANALOG_LEFT_Y) / 127.0;
		double turn = master.get_analog(ANALOG_LEFT_X)  / 127.0;
		if (master.get_digital(DIGITAL_R1)) {
			intake->intake();
		}
		else if (master.get_digital(DIGITAL_R2)) {
			intake->unstick();
		}
		else {
			intake->idle();
		}
		#define CLOSE_HIGH_SHOT 0
		#define CLOSE_LOW_SHOT 130
		#define FAR_HIGH_SHOT 65
		#define FAR_LOW_SHOT 120
		if (master.get_digital_new_press(DIGITAL_X)) {
			static float angle = CLOSE_LOW_SHOT;
			if (angle == CLOSE_LOW_SHOT) {
				angle = CLOSE_HIGH_SHOT;
			}
			else {
				angle = CLOSE_LOW_SHOT;
			}
			puncher->aimAt(angle);
		}
		
		if (master.get_digital_new_press(DIGITAL_Y)) {
			static float angle = FAR_LOW_SHOT;
			if (angle == FAR_LOW_SHOT) {
				angle = FAR_HIGH_SHOT;
			}
			else {
				angle = FAR_LOW_SHOT;
			}
			puncher->aimAt(angle);
		}

		if (master.get_digital(DIGITAL_L1)) {
			puncher->velPunch();
		}
		else {
			puncher->idle();
		}

		if (master.get_digital(DIGITAL_A)) {
			arm->articulateSpeed(0.5);
		}
		else if (master.get_digital(DIGITAL_B)) {
			arm->articulateSpeed(-0.5);
		}
		else {
			arm->articulateSpeed(0.0);
		}

		descoreArm->articulateSpeed(master.get_analog(ANALOG_RIGHT_Y) / 127.0);

		reporter->setAngle(targetAngle);
		if (master.get_digital(DIGITAL_RIGHT)) {
			drive->pointToAngle(targetAngle * okapi::degree);
		}

		drive->getChassis()->arcade(power, turn);

		pros::delay(20);
	}
}

void recvVisionData(uint8_t packetId, int32_t data) {
	if (packetId == PACKET_VISION_ANGLE) {
		targetAngle = data;
	}
	else if (packetId == PACKET_VISION_DIST) {
		targetDist = data;
	}
}
